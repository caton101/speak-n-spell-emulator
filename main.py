import subprocess
from blessed import Terminal

PROMPT = "> "

term = Terminal()

def speak(text):
    try:
        subprocess.Popen(["espeak",text], stderr=subprocess.DEVNULL)
    except FileNotFoundError:
        raise UserWarning("You need to install espeak to use this program.")


def speakIntro():
    intro = "This is a recreation of a 1970's Speak & Spell. Type anything at the prompt."
    print(intro)
    speak(intro)

def main():
    speakIntro()
    running = True
    while running:
        line = PROMPT
        while True:
            print("\r%s"%line,end="")
            key = None
            with term.raw():
                key = term.inkey(timeout=None, esc_delay=0.1)
            if key.code == term.KEY_ENTER:
                speak(line[2:])
                if line[2:] == ".exit" or line[2:] == ".quit":
                    running = False
                print()
                break
            elif key.code == term.KEY_BACKSPACE:
                if len(line) > len(PROMPT):
                    print("\r" + " "*len(line),end="")
                    line = line[:len(line)-1]
            else:
                if key == ".":
                    speak("dot")
                elif key == " ":
                    speak("space")
                elif key == ",":
                    speak("comma")
                else:
                    speak(key)
                line += key

if __name__ == "__main__":
    with term.fullscreen():
        print(term.home + term.clear,end="")
        main()